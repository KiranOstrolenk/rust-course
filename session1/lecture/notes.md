# Day 1
Commands:
``` bash
rustup default stable
rustup doc --std std::iter::Sum
cargo doc --open # docs for specific package
```

Rust won't guess function input and return type. Tell it!
`&str` - a thing you can put in double quotes

If not borrow, will consume.

Don't need return in function because functions are expressions. As is `if`. As is `loop`.

RUSTDOC: where clauses show what's need to use thing

Read to string is good