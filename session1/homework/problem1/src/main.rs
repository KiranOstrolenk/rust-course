use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).ok_or("Please give filename")?;
    let f = File::open(filename)?;
    let reader = BufReader::new(f);
    // Is there someway to use `?` instead of `expect` here?
    // Also why can't `parse` infer `i32` from the type of `sum`?
    let sum: i32 = reader.lines()
                            .map(|x| x
                                .expect("Unable to read line")
                                .parse::<i32>()
                                .expect("Unable to convert to integer")
                            )
                            .sum();
    println!("{}", sum);
    Ok(())
}