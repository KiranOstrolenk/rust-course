use std::io::BufReader;
use std::fs::File;
use std::error::Error;
use std::io::Read;

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).expect("Please give filename");
    let f = File::open(filename)?;
    let mut reader = BufReader::new(f);
    let mut file_contents = String::new();
    reader.read_to_string(&mut file_contents)?;

    let valid_brainfuck = ['>','<','+','-','.',',','[',']'];
    let brainfuck_code: String = file_contents.chars().filter(|x| valid_brainfuck.contains(x)).collect();
    println!("{}", brainfuck_code);
    
    Ok(())
}
