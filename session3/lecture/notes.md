# VSCode
* Set save on to clippy
* F2 to substitue

# Notes:
* Integration test in `tests` directory
* Gitlab pages add documentation for master branch
* Add `fmt --check` to CI
* And `clippy -- -D clippy::all`
* doc test, unit test, integration test (which are functional)
